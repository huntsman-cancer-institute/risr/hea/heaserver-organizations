# HEA Server Organization
[Research Informatics Shared Resource](https://risr.hci.utah.edu), [Huntsman Cancer Institute](https://hci.utah.edu),
Salt Lake City, UT

The HEA Server Organization is a service for managing organization information for research laboratories and other research groups.

## Version 1.6.0
* New /organizations/{id}/recentlyaccessed endpoint.

## Version 1.5.5
* Added support for python 3.12.

## Version 1.5.4
* Fixed potential hang when listing an organization's accounts.

## Version 1.5.3
* No user-visible changes. Updated to interoperate with heaserver-keychain 1.5.0.

## Version 1.5.2
* Caching optimizations.

## Version 1.5.1
* Invalidate cache when receiving collaborator changes from the message queue.

## Version 1.5.0
* Added collaborator_ids attribute. The collaborator_ids attribute is altered via message queue messages.

## Version 1.4.9
* Attempt to relink Credentials objects with their corresponding Volumes when saving an Organization.

## Version 1.4.8
* Handle case where user who is an organization admin, manager, member, or PI has been deleted from keycloak.
* Omit system users from the membership editor form.

## Version 1.4.7
* Fixed issue causing volumes and credentials for a new PI not to be created.

## Version 1.4.6
* Fixed issue creating new credentials objects without a role when a user is added to an organization.

## Version 1.4.5
* Fixed crash when reusing an existing volume and credentials when a user's organization membership has changed.

## Version 1.4.4
* Return 400 status code when the user lacks permission to update permissions for an organization (those errors were ignored previously).
* Corrected permissions checking for organization managers and members.

## Version 1.4.3
* Send the account ids and group id fields when submitting the members editor form.

## Version 1.4.2
* Corrected heaobject.organization.Organization permissions.

## Version 1.4.1
* Generate the correct name attribute for heaobject.keychain.AWSCredentials objects.

## Version 1.4.0
* Changing organization members, managers, admins, and PI now changes the users' volumes and credentials objects to
match.
* The admin, manager, and member lists now omit system users.

## Version 1.3.0
* Display type display name in properties card, and return the type display name from GET calls.

## Version 1.2.0
* Improved performance accessing an organization's AWS accounts.
* heaobject.organization.Organization objects now have an accounts attribute that mirrors the contents of the existing
aws_account_ids attribute and will support other heaobject.account.Account subclasses in the future.

## Version 1.1.0
* Pass desktop object permissions back to clients.

## Version 1.0.2
* Improved performance.

## Version 1.0.1
* Improved performance.

## Version 1
Initial release.

## Runtime requirements
* Python 3.10, 3.11, or 3.12.

## Development environment

### Build requirements
* Any development environment is fine.
* On Windows, you also will need:
    * Build Tools for Visual Studio 2019, found at https://visualstudio.microsoft.com/downloads/. Select the C++ tools.
    * git, found at https://git-scm.com/download/win.
* On Mac, Xcode or the command line developer tools is required, found in the Apple Store app.
* Python 3.10, 3.11, or 3.12: Download and install Python from https://www.python.org, and select the options to install
for all users and add Python to your environment variables. The install for all users option will help keep you from
accidentally installing packages into your Python installation's site-packages directory instead of to your virtualenv
environment, described below.
* Create a virtualenv environment using the `python -m venv <venv_directory>` command, substituting `<venv_directory>`
with the directory name of your virtual environment. Run `source <venv_directory>/bin/activate` (or `<venv_directory>/Scripts/activate` on Windows) to activate the virtual
environment. You will need to activate the virtualenv every time before starting work, or your IDE may be able to do
this for you automatically. **Note that PyCharm will do this for you, but you have to create a new Terminal panel
after you newly configure a project with your virtualenv.**
* From the project's root directory, and using the activated virtualenv, run
  `pip install -r requirements_dev.txt`. **Do NOT run `python setup.py develop`. It will break your environment.**

### Running tests
Run tests with the `pytest` command from the project root directory. To improve performance, run tests in multiple
processes with `pytest -n auto`.

### Running integration tests
* Install Docker
* On Windows, install pywin32 version >= 223 from https://github.com/mhammond/pywin32/releases. In your venv, make sure that
`include-system-site-packages` is set to `true`.

### Trying out the APIs
This microservice has Swagger3/OpenAPI support so that you can quickly test the APIs in a web browser. Do the following:
* Install Docker, if it is not installed already.
* Run the `run-swaggerui.py` file in your terminal. This file contains some test objects that are loaded into a MongoDB
  Docker container.
* Go to `http://127.0.0.1:8080/docs` in your web browser.

Once `run-swaggerui.py` is running, you can also access the APIs via `curl` or other tool. For example, in Windows
PowerShell, execute:
```
Invoke-RestMethod -Uri http://localhost:8080/organizations/ -Method GET -Headers @{'accept' = 'application/json'}`
```
In MacOS or Linux, the equivalent command is:
```
curl -X GET http://localhost:8080/organizations/ -H 'accept: application/json'
```

### Packaging and releasing this project
See the [RELEASING.md](RELEASING.md) file for details.
