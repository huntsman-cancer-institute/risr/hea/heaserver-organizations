# When updating package versions, also update the versions of pytest, pytest-xdist, and mypy in HEA Main in the
# heaserver-organizations Dockerfile.
setuptools~=75.2.0
-e . # useful for getting app to recognize the heaserver-organization module in all files in the project
pytest~=8.3.3
testcontainers~=4.8.2
twine~=5.1.1
mypy==1.11.1
pip-licenses~=5.0.0
aiohttp-swagger3~=0.9.0
moto[s3,sts,organizations]~=5.0.18  # Needed to use accounts during integration testing.
pytest-xdist[psutil]~=3.6.1
pytest-cov~=5.0.0
build~=1.2.2.post1
pygount~=1.8.0
